'use strict'

const axios = require('axios');


let config = {
    baseURL: process.env.REST_ENDPOINT + process.env.API_VERSION,
    // timeout: 60 * 1000, // Timeout
    // withCredentials: true, // Check cross-site Access-Control
    // baseURL: 'http://localhost:8000',
    timeout: 3600 // Timeout
}

const _axios = axios.create(config)

_axios.interceptors.request.use(
    function (config) {

        // config.headers['Authorization'] = 'Bearer ' + token
        // Do something before request is sent
        return config
    },
    function (error) {
        // Do something with request error
        return Promise.reject(error)
    }
)

// Add a response interceptor
_axios.interceptors.response.use(
    function (response) {
        // Do something with response data
        return response.data
    },
    function (error) {
        console.log(error)
        // Do something with response error
        return Promise.reject(error)
    }
)

module.exports = _axios